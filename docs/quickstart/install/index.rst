Install
=======

Idem is supported on Linux, macOS and Windows.

Choose your operating system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. grid:: 2

    .. grid-item-card:: Linux Packages
        :link: linux
        :link-type: doc

        Install on Linux with system packages

        :bdg-info:`Linux`

    .. grid-item-card:: Windows Installer
        :link: windows
        :link-type: doc

        Windows Installer

        :bdg-info:`Windows`

    .. grid-item-card:: macOS
        :link: macos
        :link-type: doc

        macOS

        :bdg-info:`macOS`

    .. grid-item-card:: python-pip
        :link: pythonpip
        :link-type: doc

        Install with Python Pip

        :bdg-info:`pip`

    .. grid-item-card:: idembinary
        :link: idembinary
        :link-type: doc

        Use Idem binary

        :bdg-info:`binary`


.. toctree::
   :maxdepth: 1
   :hidden:

   linux
   macos
   windows
   macos
   idembinary
   pythonpip
